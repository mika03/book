package com.example.book.entities;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
public abstract class BaseEntity implements Serializable {
    protected LocalDateTime createdDate;
    protected LocalDateTime updatedDate;
    protected String createdUser;
    protected String updatedUser;
}
