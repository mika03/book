package com.example.book.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "RentalBook")
public class RentalBook extends BaseEntity {
    @Id
    private ObjectId id;
    // Id sách
    private ObjectId bookId;
    // Người mượn
    private ObjectId userId;
    // Tiền nợ
    private Long debtPrice;
    // Tiền trả
    private Long returnedPrice;
    // Ngày thuê
    private LocalDateTime rentalDate;
    // Ngày trả
    private LocalDateTime returnedDate;
}
