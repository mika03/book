package com.example.book.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "BookCategory")
public class BookCategory extends BaseEntity {
    @Id
    private ObjectId id;
    // Tên thể loại sách
    private String name;
    // Mô tả thể loại sách
    private String description;
}
