package com.example.book.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "Book")
public class Book extends BaseEntity {
    @Id
    private ObjectId id;
    // Tên sách
    private String name;
    // Mô tả sách
    private String description;
    // Số trang sách
    private Integer bookPages;
    // Id thể loại sách
    private ObjectId bookCategoryId;
    // Giá thuê
    private Long rentalPrice;
    // Số quyển
    private Integer number;
}
