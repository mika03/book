package com.example.book.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "User")
public class User extends BaseEntity {
    @Id
    private ObjectId id;
    // Tên người
    private String name;
    // Số tiền của người
    private Long price;
}
