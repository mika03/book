package com.example.book.controllers;

import com.example.book.DTOs.ResultDTO;
import com.example.book.DTOs.request.UserRequestDTO;
import com.example.book.services.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin("*")
@Tag(name = "User API")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    private ResultDTO create(@RequestBody UserRequestDTO dto) {
        return userService.create(dto);
    }
}
