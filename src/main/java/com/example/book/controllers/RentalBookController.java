package com.example.book.controllers;

import com.example.book.DTOs.ResultDTO;
import com.example.book.DTOs.request.RentalBookRequestDTO;
import com.example.book.services.RentalBookService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/rental-book", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin("*")
@Tag(name = "Rental Book API")
public class RentalBookController {
    @Autowired
    private RentalBookService rentalBookService;

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    private ResultDTO create(@RequestBody RentalBookRequestDTO dto) {
        return rentalBookService.create(dto);
    }

    @RequestMapping(value = "/get-all", method = RequestMethod.GET)
    private ResultDTO getAll() {
        return rentalBookService.getAll();
    }
}
