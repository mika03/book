package com.example.book.controllers;

import com.example.book.DTOs.ResultDTO;
import com.example.book.DTOs.request.BookCategoryRequestDTO;
import com.example.book.services.BookCategoryService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/book-category", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin("*")
@Tag(name = "Book Category API")
public class BookCategoryController {
    @Autowired
    private BookCategoryService bookCategoryService;

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    private ResultDTO create(@RequestBody BookCategoryRequestDTO dto) {
        return bookCategoryService.create(dto);
    }
}
