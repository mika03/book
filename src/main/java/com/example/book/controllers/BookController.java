package com.example.book.controllers;

import com.example.book.DTOs.ResultDTO;
import com.example.book.DTOs.request.BookRequestDTO;
import com.example.book.services.BookService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/book", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin("*")
@Tag(name = "Book API")
public class BookController {
    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    private ResultDTO create(@RequestBody BookRequestDTO dto) {
        return bookService.create(dto);
    }
}
