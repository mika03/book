package com.example.book.repositories;

import com.example.book.entities.RentalBook;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RentalBookRepository extends MongoRepository<RentalBook, ObjectId> {
}
