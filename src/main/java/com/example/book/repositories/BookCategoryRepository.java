package com.example.book.repositories;

import com.example.book.entities.BookCategory;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookCategoryRepository extends MongoRepository<BookCategory, ObjectId> {
    BookCategory getById(ObjectId id);
}
