package com.example.book.constants;

public class ResultConstant {
    public static String SUCCESS = "200";
    public static String FAIL = "400";

    public static String DES_SUCCESS = "Success";
    public static String DES_FAIL = "Fail";
    public static String DES_BOOK_CATEGORY_NOT_EXIST = "No book category";
    public static String DES_USER_NOT_EXIST = "No user";
    public static String DES_BOOK_NOT_EXIST = "No book";
    public static String DES_MONEY_NOT_ENOUGH = "Not enough money";
}
