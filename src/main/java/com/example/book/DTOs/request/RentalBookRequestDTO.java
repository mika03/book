package com.example.book.DTOs.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RentalBookRequestDTO {
    private String bookId;
    private String userId;
    private Long returnedPrice;
    private LocalDateTime rentalDate;
    private LocalDateTime returnedDate;
}
