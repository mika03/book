package com.example.book.DTOs.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookRequestDTO {
    private String name;
    private String description;
    private Integer bookPages;
    private String bookCategoryId;
    private Long rentalPrice;
    private Integer number;
}
