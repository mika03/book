package com.example.book.DTOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RentalBookInfoDTO {
    private String id;
    private String bookId;
    private String userId;
    private Long debtPrice;
    private Long returnedPrice;
    private LocalDateTime rentalDate;
    private LocalDateTime returnedDate;
    private LocalDateTime lateDate;
}
