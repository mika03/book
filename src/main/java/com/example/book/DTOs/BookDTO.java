package com.example.book.DTOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookDTO {
    private String id;
    private String name;
    private String description;
    private Integer bookPages;
    private String bookCategoryId;
    private Long rentalPrice;
    private Integer number;
}
