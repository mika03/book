package com.example.book.DTOs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResultDTO<T> {
    private String status;
    private String description;
    private T data;

    public ResultDTO() {}
    public ResultDTO(String status, String description) {
        this.status = status;
        this.description = description;
    }

    public ResultDTO(String status, String description, T data) {
        this.status = status;
        this.description = description;
        this.data = data;
    }
}
