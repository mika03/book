package com.example.book.services;

import com.example.book.DTOs.ResultDTO;
import com.example.book.DTOs.request.BookCategoryRequestDTO;

public interface BookCategoryService {
    ResultDTO create(BookCategoryRequestDTO dto);
}
