package com.example.book.services.common;

import com.example.book.entities.Book;
import com.example.book.entities.BookCategory;
import com.example.book.entities.User;
import com.example.book.repositories.BookCategoryRepository;
import com.example.book.repositories.BookRepository;
import com.example.book.repositories.RentalBookRepository;
import com.example.book.repositories.UserRepository;
import org.bson.types.ObjectId;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseService {

    @Autowired
    protected BookRepository bookRepository;

    @Autowired
    protected BookCategoryRepository bookCategoryRepository;

    @Autowired
    protected RentalBookRepository rentalBookRepository;

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected DozerBeanMapper mapper;

    protected Boolean checkBookCategory(ObjectId id) {
        BookCategory data = bookCategoryRepository.getById(id);
        if (data == null)
            return true;

        return false;
    }

    protected Boolean checkBook(ObjectId id) {
        Book data = bookRepository.getById(id);
        if (data == null)
            return true;

        return false;
    }

    protected Boolean checkUser(ObjectId id) {
        User data = userRepository.getById(id);
        if (data == null)
            return true;

        return false;
    }

    protected User getUserById(ObjectId id) {
        User data = userRepository.getById(id);
        return data;
    }

    protected Book getBookById(ObjectId id) {
        Book data = bookRepository.getById(id);
        return data;
    }
}
