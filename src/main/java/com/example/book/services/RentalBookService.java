package com.example.book.services;

import com.example.book.DTOs.ResultDTO;
import com.example.book.DTOs.request.RentalBookRequestDTO;

public interface RentalBookService {
    ResultDTO create(RentalBookRequestDTO dto);
    ResultDTO getAll();
}
