package com.example.book.services.impl;

import com.example.book.DTOs.BookCategoryDTO;
import com.example.book.DTOs.ResultDTO;
import com.example.book.DTOs.request.BookCategoryRequestDTO;
import com.example.book.constants.ResultConstant;
import com.example.book.entities.BookCategory;
import com.example.book.services.BookCategoryService;
import com.example.book.services.common.BaseService;
import org.springframework.stereotype.Service;

@Service
public class BookCategoryServiceImpl extends BaseService implements BookCategoryService {

    @Override
    public ResultDTO create(BookCategoryRequestDTO dto) {
        BookCategory bookCategory = mapper.map(dto, BookCategory.class);
        bookCategory = bookCategoryRepository.save(bookCategory);
        return new ResultDTO(ResultConstant.SUCCESS, ResultConstant.DES_SUCCESS, mapper.map(bookCategory, BookCategoryDTO.class));
    }
}
