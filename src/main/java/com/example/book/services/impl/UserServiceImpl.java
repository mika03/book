package com.example.book.services.impl;

import com.example.book.DTOs.ResultDTO;
import com.example.book.DTOs.UserDTO;
import com.example.book.DTOs.request.UserRequestDTO;
import com.example.book.constants.ResultConstant;
import com.example.book.entities.User;
import com.example.book.services.UserService;
import com.example.book.services.common.BaseService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends BaseService implements UserService {

    @Override
    public ResultDTO create(UserRequestDTO dto) {
        User user = mapper.map(dto, User.class);
        user = userRepository.save(user);
        return new ResultDTO(ResultConstant.SUCCESS, ResultConstant.DES_SUCCESS, mapper.map(user, UserDTO.class));
    }
}
