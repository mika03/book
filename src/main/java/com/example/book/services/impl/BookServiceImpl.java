package com.example.book.services.impl;

import com.example.book.DTOs.BookDTO;
import com.example.book.DTOs.ResultDTO;
import com.example.book.DTOs.request.BookRequestDTO;
import com.example.book.constants.ResultConstant;
import com.example.book.entities.Book;
import com.example.book.services.BookService;
import com.example.book.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl extends BaseService implements BookService {

    @Override
    public ResultDTO create(BookRequestDTO dto) {
        if (checkBookCategory(new ObjectId(dto.getBookCategoryId())) == true)
            return new ResultDTO(ResultConstant.FAIL, ResultConstant.DES_BOOK_CATEGORY_NOT_EXIST);

        Book book = mapper.map(dto, Book.class);
        book = bookRepository.save(book);
        return new ResultDTO(ResultConstant.SUCCESS, ResultConstant.DES_SUCCESS, mapper.map(book, BookDTO.class));
    }
}
