package com.example.book.services.impl;

import com.example.book.DTOs.RentalBookDTO;
import com.example.book.DTOs.RentalBookInfoDTO;
import com.example.book.DTOs.ResultDTO;
import com.example.book.DTOs.request.RentalBookRequestDTO;
import com.example.book.constants.ResultConstant;
import com.example.book.entities.RentalBook;
import com.example.book.entities.User;
import com.example.book.services.RentalBookService;
import com.example.book.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RentalBookServiceImpl extends BaseService implements RentalBookService {

    @Override
    public ResultDTO create(RentalBookRequestDTO dto) {
        if (checkUser(new ObjectId(dto.getUserId())) == true)
            return new ResultDTO(ResultConstant.FAIL, ResultConstant.DES_USER_NOT_EXIST);

        if (checkBook(new ObjectId(dto.getBookId())) == true)
            return new ResultDTO(ResultConstant.FAIL, ResultConstant.DES_BOOK_NOT_EXIST);

        RentalBook rentalBook = new RentalBook();
        rentalBook.setBookId(new ObjectId(dto.getBookId()));
        rentalBook.setUserId(new ObjectId(dto.getUserId()));
        rentalBook.setReturnedPrice(dto.getReturnedPrice());
        rentalBook.setRentalDate(dto.getRentalDate());
        rentalBook.setReturnedDate(dto.getReturnedDate());
        Map<String, String> getPrice = getPrice(getUserById(new ObjectId(dto.getUserId())).getPrice(),
                                        0L, dto.getReturnedPrice(),
                                        getBookById(new ObjectId(dto.getBookId())).getRentalPrice());
        rentalBook.setDebtPrice(Long.valueOf(getPrice.get("debtPrice")));
        User user = getUserById(new ObjectId(dto.getUserId()));
        user.setPrice(Long.valueOf(getPrice.get("price")));
        userRepository.save(user);
        rentalBook = rentalBookRepository.save(rentalBook);
        RentalBookDTO rentalBookDTO = new RentalBookDTO();
        rentalBookDTO.setId(rentalBook.getId().toString());
        rentalBookDTO.setBookId(rentalBook.getBookId().toString());
        rentalBookDTO.setUserId(rentalBook.getUserId().toString());
        rentalBookDTO.setDebtPrice(rentalBook.getDebtPrice());
        rentalBookDTO.setReturnedDate(rentalBook.getReturnedDate());
        rentalBookDTO.setRentalDate(rentalBook.getRentalDate());
        rentalBookDTO.setReturnedPrice(rentalBook.getReturnedPrice());
        return new ResultDTO(ResultConstant.SUCCESS, ResultConstant.DES_SUCCESS, rentalBookDTO);
    }

    @Override
    public ResultDTO getAll() {
        List<RentalBook> rentalBooks = rentalBookRepository.findAll();
        List<RentalBookInfoDTO> response = new ArrayList<>();
        rentalBooks.forEach(x -> {
            RentalBookInfoDTO rentalBookInfoDTO = new RentalBookInfoDTO();
            rentalBookInfoDTO.setId(x.getId().toString());
            rentalBookInfoDTO.setBookId(x.getBookId().toString());
            rentalBookInfoDTO.setUserId(x.getUserId().toString());
            rentalBookInfoDTO.setDebtPrice(x.getDebtPrice());
            rentalBookInfoDTO.setReturnedPrice(x.getReturnedPrice());
            rentalBookInfoDTO.setRentalDate(x.getRentalDate());
            rentalBookInfoDTO.setReturnedDate(x.getReturnedDate());
            LocalDateTime lateDate = LocalDateTime.now().minusYears(x.getReturnedDate().getYear()).minusDays(x.getReturnedDate().
                                        getDayOfYear()).minusHours(x.getReturnedDate().getHour()).minusMinutes(x.getReturnedDate().getMinute());
            rentalBookInfoDTO.setLateDate(lateDate);
            response.add(rentalBookInfoDTO);
        });
        return new ResultDTO(ResultConstant.SUCCESS, ResultConstant.DES_SUCCESS, response);
    }

    private Map<String, String> getPrice(Long price, Long debtPrice, Long returnedPrice, Long rentalPrice) {
        Map<String, String> response = new HashMap<>();
        if (price.longValue() == 0) {
            long checkPrice = returnedPrice.longValue() - rentalPrice.longValue();
            if (checkPrice < 0)
                debtPrice = checkPrice;
            else
                debtPrice = 0L;
        } else {
            price = price.longValue() - returnedPrice.longValue();
        }
        response.put("price", price.toString());
        response.put("debtPrice", debtPrice.toString());
        return response;
    }
}
