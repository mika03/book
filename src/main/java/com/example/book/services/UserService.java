package com.example.book.services;

import com.example.book.DTOs.ResultDTO;
import com.example.book.DTOs.request.UserRequestDTO;

public interface UserService {
    ResultDTO create(UserRequestDTO dto);
}
