package com.example.book.services;

import com.example.book.DTOs.ResultDTO;
import com.example.book.DTOs.request.BookRequestDTO;

public interface BookService {
    ResultDTO create(BookRequestDTO dto);
}
